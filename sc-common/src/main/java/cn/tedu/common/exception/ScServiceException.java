package cn.tedu.common.exception;

import cn.tedu.common.vo.ResponseCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ScServiceException extends RuntimeException{

    private ResponseCode responseCode;

    public ScServiceException(ResponseCode responseCode, String message) {
        super(message);
        setResponseCode(responseCode);
    }
    public ScServiceException() {
        super();
    }

    public ScServiceException(String message) {
        super(message);
    }

    public ScServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScServiceException(Throwable cause) {
        super(cause);
    }

    protected ScServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
