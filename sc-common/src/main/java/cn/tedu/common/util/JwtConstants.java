package cn.tedu.common.util;

/**
 * @author Kiven
 */
public class JwtConstants {
    public static final long REFRESH_TIME = 10*24*60*60*1000L;

    public static final String AUTHORIZATION = "Authorization";

    public static final String AUTHORIZATION_BEARER = "Bearer ";

    public static final String REFRESH_TOKEN_HEADER_NAME = "refresh_token";

    public static final Integer JWT_TIMEOUT = 30;

    public static final String SHOP_ID = "shopId";


    public static final String USER_ID = "id";

    public static final String PHONE ="phone";
}
