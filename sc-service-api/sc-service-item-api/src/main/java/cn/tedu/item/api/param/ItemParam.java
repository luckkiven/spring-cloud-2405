package cn.tedu.item.api.param;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class ItemParam {
    private String name;
    private BigDecimal price;
}
