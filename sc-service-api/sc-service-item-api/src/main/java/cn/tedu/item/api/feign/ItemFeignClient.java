package cn.tedu.item.api.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "item-service")
public interface ItemFeignClient {

    @GetMapping("/font/item")
    public String sayHello();

}
