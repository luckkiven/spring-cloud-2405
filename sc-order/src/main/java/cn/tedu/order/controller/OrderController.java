package cn.tedu.order.controller;

import cn.tedu.item.api.feign.ItemFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/font/order")
//@CrossOrigin
public class OrderController {
    @Autowired
    private ItemFeignClient service;
    @GetMapping
    public String say(){
        return service.sayHello();
    }
}
