package cn.tedu.commondb.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"cn.tedu.**.mapper"})
public class MybatisConfiguration {
}
