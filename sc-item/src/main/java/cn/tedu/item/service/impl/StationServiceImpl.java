package cn.tedu.item.service.impl;

import cn.tedu.item.mapper.StationMapper;
import cn.tedu.item.pojo.entity.Station;
import cn.tedu.item.service.IStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StationServiceImpl implements IStationService {

    @Autowired
    private StationMapper stationMapper;

    @Override
    public List<Station> getAllStations() {
        return stationMapper.findAll();
    }
}
