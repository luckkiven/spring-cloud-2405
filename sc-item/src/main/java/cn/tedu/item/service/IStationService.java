package cn.tedu.item.service;

import cn.tedu.item.pojo.entity.Station;
import org.springframework.stereotype.Service;

import java.util.List;


public interface IStationService {



    public List<Station> getAllStations();

}
