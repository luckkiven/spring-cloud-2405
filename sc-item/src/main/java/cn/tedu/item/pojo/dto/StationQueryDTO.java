package cn.tedu.item.pojo.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiModel(value = "StationDTO", description = "描述充电站信息")
@Data
public class StationQueryDTO {

    @ApiModelProperty(value = "站点代码", required = true)
    @Size(max = 10, message = "站点代码不能超过10个字符")
    private String stationCode;

    @ApiModelProperty(value = "站点名称")
    @Size(max = 10, message = "站点名称不能超过10个字符")
    private String stationName;

    @ApiModelProperty(value = "设备编号")
    private Integer deviceNumber;

    @ApiModelProperty(value = "交流枪口数量")
    @Min(value = 0, message = "交流枪口数量不能小于0")
    private Integer acGunNumber;

    @ApiModelProperty(value = "直流枪口数量")
    @Min(value = 0, message = "直流枪口数量不能小于0")
    private Integer dcGunNumber;

    @ApiModelProperty(value = "交流额定功率")
    private Integer acRatePower;

    @ApiModelProperty(value = "直流额定功率")
    private Integer dcRatePower;

    @ApiModelProperty(value = "省份")
    @Size(max = 8, message = "省份长度不能超过8个字符")
    private String province;

    @ApiModelProperty(value = "城市")
    @Size(max = 8, message = "城市长度不能超过8个字符")
    private String city;

    @ApiModelProperty(value = "地址")
    @Size(max = 50, message = "地址长度不能超过50个字符")
    private String address;

    @ApiModelProperty(value = "站点状态")
    private Byte stationStatus;

    @ApiModelProperty(value = "站点类型")
    private Byte stationType;

    @ApiModelProperty(value = "站点最后修改时间")
    private Date stationModified;

    @ApiModelProperty(value = "站点创建时间")
    private Date stationCreate;

    @ApiModelProperty(value = "操作员ID")
    private Integer operatorId;


}
