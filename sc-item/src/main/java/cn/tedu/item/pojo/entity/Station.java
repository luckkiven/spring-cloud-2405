package cn.tedu.item.pojo.entity;
import lombok.Data;

import java.util.Date;

@Data
public class Station {

    private Integer id;
    private String stationCode;
    private String stationName;
    private Integer deviceNumber;
    private Integer acGunNumber;
    private Integer dcGunNumber;
    private Integer acRatePower;
    private Integer dcRatePower;
    private String province;
    private String city;
    private String address;
    private Double stationLng;
    private Double stationLat;
    private Integer devicePower;
    private Byte stationModel;
    private Byte stationStatus;
    private Byte stationType;
    private Date stationModified;
    private Date stationCreate;
    private Integer operatorId;
    private Double parkFee;

}
