package cn.tedu.item.controller;


import cn.tedu.item.pojo.dto.StationQueryDTO;
import cn.tedu.item.pojo.entity.Station;
import cn.tedu.item.service.IStationService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 充电场站
 */
@Validated
@RestController
@RequestMapping("/app/station")
@Slf4j
@Api(tags = "充电场站模块")
public class StationController {
    @Autowired
    private IStationService stationService;

    @GetMapping
    public List<Station> getAllStations(StationQueryDTO stationQueryDTO) {
        return stationService.getAllStations();
    }
}
