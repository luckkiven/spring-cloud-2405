package cn.tedu.item.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/font/item")
@RestController
public class ItemController {
    @GetMapping
    public String sayHello(){
        return "hello";
    }
}
