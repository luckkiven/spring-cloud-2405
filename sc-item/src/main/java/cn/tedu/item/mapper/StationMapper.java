package cn.tedu.item.mapper;

import cn.tedu.item.pojo.entity.Station;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface StationMapper {

    @Select("SELECT * FROM charging_station")
    List<Station> findAll();

    // Additional CRUD operations
}
