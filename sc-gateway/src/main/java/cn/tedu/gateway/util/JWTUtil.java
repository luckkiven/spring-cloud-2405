package cn.tedu.gateway.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 封装生成token和验证token方法
 */
@Component
public class JWTUtil {
    /**
     * 盐值
     * 生成token header.payload.signature
     */
    private static String SIGNATURE="token!Q2W#E$RW";




    /**
     * 生成token
     * @param map
     * @return
     */
    public static String getToken(Map<String,Object> map){
        Calendar instance = Calendar.getInstance();
        //30分钟过期
        instance.add(Calendar.MINUTE, 30);
        //创建jwt builder
        JWTCreator.Builder builder = JWT.create();

        //payload,用遍历避免多个键值对
        map.forEach((k,v)->{
             builder.withClaim(k,v+"");
        });
        //指定令牌过期时间,设置签名返回token
        String token = builder.withExpiresAt(instance.getTime())
                .sign(Algorithm.HMAC256(SIGNATURE));
        return token;
    }

    /**
     * 验证token合法性
     * @param token
     */
    public static void verify(String token){
        JWT.require(Algorithm.HMAC256(SIGNATURE)).build().verify(token);
    }

    public static void main(String[] args) {
        /*Map<String,Object> map = new HashMap<>();
        map.put("username", "jx");
        String token = getToken(map);
        System.out.println(token);*/

        String token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MjQ4MzAzMjcsInVzZXJuYW1lIjoiangifQ.YADLW29ivBoBHmwxWQPhUKcomPxgFFV-Gb1Y2gufsN0";
        verify(token);
        System.out.println("ok");


        //Date date = new Date(); // 17.00
        //
        //long 过期时间 = date.getTime();
        //过期时间 += 2*60*1000;  // 17.02 的毫秒值保存
        //// 17.04分了
        //long l = System.currentTimeMillis();
        //if(l>过期时间){
        //    throw  new RuntimeException("已过期");
        //}


    }






















    public static DecodedJWT getTokenInfo(String token ){
        return   JWT.require(Algorithm.HMAC256(SIGNATURE)).build().verify(token);

    }

    public static Map<String,Object> getUserInfo(String token){
        DecodedJWT tokenInfo = JWT.require(Algorithm.HMAC256(SIGNATURE)).build().verify(token);
        Map<String, Claim> claims = tokenInfo.getClaims();
        Set<Map.Entry<String, Claim>> entries = claims.entrySet();
        Map<String,Object> map = new HashMap<>();
        for (Map.Entry<String, Claim> entry : entries) {
            map.put(entry.getKey(),entry.getValue().as(Object.class));
        }
        //SpsAdminEntity spsAdminEntity = JSON.parseObject(JSON.toJSONString(map), SpsAdminEntity.class);
        return map;
    }


}
