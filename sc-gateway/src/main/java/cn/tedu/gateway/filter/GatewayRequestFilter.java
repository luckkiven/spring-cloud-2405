//package cn.tedu.gateway.filter;
//
//import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.cloud.gateway.filter.GlobalFilter;
//import org.springframework.core.Ordered;
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.http.server.reactive.ServerHttpResponse;
//import org.springframework.stereotype.Component;
//import org.springframework.web.server.ServerWebExchange;
//import reactor.core.publisher.Mono;
//
///**
// * @Author: czm
// * @Date: 2022/04/14/15:29
// * @Description:用于请求的全局过滤器
// */
//@Component
//public class GatewayRequestFilter implements GlobalFilter, Ordered {
//
//    @Override
//    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//        //获取请求与响应
//        ServerHttpRequest request = exchange.getRequest();
//        ServerHttpResponse response = exchange.getResponse();
//
//        //放行不需要登录路由
//        String uri = request.getURI().getPath();
//        System.out.println(uri);
//        //String token = request.getHeaders().getFirst(SecurityConstants.AUTHORIZATION_KEY);
//        //if(StringUtils.isEmpty(token)){
//        //    response.setStatusCode(HttpStatus.UNAUTHORIZED);
//        //    response.setComplete();
//        //    return chain.filter(exchange);
//        //}
//        //String substringToken = token.substring(7);
//        //System.out.println(substringToken);
//        // 续命
//        //Long expire = redisTemplate.getExpire(substringToken);
//        //if (expire!=-2){
//        //    boolean expired = JWTUtil.isExpired(token);
//        //    if (!expired){
//        //        return chain.filter(exchange);
//        //    }
//        //    String s = redisTemplate.opsForValue().get(token);
//        //    String newToken = authFegin.refreshToken(Integer.valueOf(s));
//        //    redisTemplate.opsForValue().set(newToken,s, Duration.ofHours(3));
//        //    response.getHeaders().add("token",newToken);
//        //    response.getHeaders().set("Access-Control-Expose-Headers","token");
//        //    response.setComplete();
//        //    return chain.filter(exchange);
//        //}
//        //response.setStatusCode(HttpStatus.UNAUTHORIZED);
//        //response.setComplete();
//        return chain.filter(exchange);
//    }
//
//    @Override
//    public int getOrder() {
//        return 0;
//    }
//}
