package cn.tedu.gateway.filter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
public class JwtFilter implements GlobalFilter , Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String uri = request.getURI().toString();
        // 记得截取 Breaer 空格
        String token = request.getHeaders().getFirst("Authorization");
        if("options".equals(request.getMethod())){
            return chain.filter(exchange);
        }
        if(StringUtils.isBlank(token)){
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return  response.setComplete();
        }
        //try{
        //    JWTUtil.verify(token);
        //}catch (Exception e){
        //    response.setStatusCode(HttpStatus.UNAUTHORIZED);
        //    return response.setComplete();
        //}
        System.out.println("I`m过滤器1"+uri);

        //代表通过
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
