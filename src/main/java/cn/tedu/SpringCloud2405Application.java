package cn.tedu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloud2405Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloud2405Application.class, args);
    }

}
